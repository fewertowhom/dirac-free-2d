# dirac-free-2d

Simple code to compute the Euclidean time correlator for zero momentum two-dimensional fermions.

## Dependencies
- [Eigen](https://eigen.tuxfamily.org/)

## Installation
Change the path to the Eigen library on ```Makefile```, then
```
make
./run-dirac
```

## Run parameters
At the top of the source file ```dirac2d.cpp``` you will find the following parameters:
```
int constexpr maxIterations = 50000;
double constexpr CGtolerance = 1e-9;
double constexpr mass = 0.0537823923;
double constexpr r = 1.0;
```
They correspond to, in this order:
- Maximum number of iterations used by the CG algorithm;
- Targe tolerance of CG;
- Bare fermion mass;
- Wilson parameter.

Additionally, I have implemented the option to run the code in the Dirac basis, as well as the chiral basis. In order to switch to the Dirac basis, comment out the following line:
```
#define CHIRAL_BASIS
```
