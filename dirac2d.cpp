#include <vector>
#include <numeric>
#include <iostream>
#include "Eigen/Core"

int constexpr maxIterations = 50000;
double constexpr CGtolerance = 1e-9;
double constexpr mass = 0.0537823923;
double constexpr r = 1.0;

using spinor = Eigen::Vector2d;
using mat = Eigen::Matrix2d;

#define CHIRAL_BASIS

#ifdef CHIRAL_BASIS
// gamma matrices 
mat const gamma_t  = mat{{0.0, 1.0}, {1.0, 0.0}};
mat const gamma_x  = mat{{0.0, -1.0}, {1.0,  0.0}};
// helicity eigenstates
auto const s1 = spinor{1.0, 0.0};
auto const s2 = spinor{0.0, 1.0};
#else
// gamma matrices 
mat const gamma_t  = mat{{1.0, 0.0}, {0.0, -1.0}};
mat const gamma_x  = mat{{0.0, 1.0}, {1.0,  0.0}};
// helicity eigenstates
auto const s1 = spinor{1.0 / M_SQRT2, 1.0 / M_SQRT2};
auto const s2 = spinor{1.0 / M_SQRT2, -1.0 / M_SQRT2};
#endif

mat const identity = mat{{1.0, 0.0}, {0.0,  1.0}};

struct Field {
	int const Nt, Nx;
	std::vector<spinor> field;

	int index(int t, int x) const {
		if (t < 0) t += Nt;
		if (x < 0) x += Nx;
		return (t % Nt) * Nx + (x % Nx);
	}

	Field(int const nt, int const nx)
	: Nt{nt}
	, Nx{nx}
	, field{std::vector<spinor>(Nt*Nx)}
	{ }

	spinor  operator()(int const t, int const x) const { return field[index(t, x)]; }
	spinor& operator()(int const t, int const x)       { return field[index(t, x)]; }

	double Dot(Field const& other) const {
		double out = 0.0;
		for (int t = 0; t < Nt; ++t)
			for (int x = 0; x < Nx; ++x)
				out += field[index(t, x)].adjoint() * other(t, x);
		return out;
	}
};

void applyDirac(Field const& in, Field& out) {
	for (int t = 0; t < in.Nt; ++t) {
		int const factorPrev = ((t - 1 < 0)      ? -1 : 1);
		int const factorNext = ((t + 1 >= in.Nt) ? -1 : 1);
		for (int x = 0; x < in.Nx; ++x) {
			out(t, x) = (2.0 * r + mass) * in(t, x)
				- 0.5 * ((r * identity - gamma_t) * in(t+1, x) * factorNext
						+ (r * identity + gamma_t) * in(t-1,x) * factorPrev)
				- 0.5 * ((r * identity - gamma_x) * in(t, x+1)
						+ (r * identity + gamma_x) * in(t,x-1));
		}
	}
}

void applyDiracDagger(Field const& in, Field& out) {
	for (int t = 0; t < in.Nt; ++t) {
		int const factorPrev = ((t - 1 < 0)      ? -1 : 1);
		int const factorNext = ((t + 1 >= in.Nt) ? -1 : 1);
		for (int x = 0; x < in.Nx; ++x) {
			out(t, x) = (2.0 * r + mass) * in(t, x)
				- 0.5 * ((r * identity - gamma_t).adjoint() * in(t-1, x) * factorPrev
						+ (r * identity + gamma_t).adjoint() * in(t+1,x) * factorNext)
				- 0.5 * ((r * identity - gamma_x).adjoint() * in(t, x-1)
						+ (r * identity + gamma_x).adjoint() * in(t,x+1));
		}
	}
}

int ConjugateGradient(Field const& in, Field& out) {
	double norm = 1.0;
	int iterCount = 0;
	Field r = in;
	Field p = r;
	auto Ap = Field{in.Nt, in.Nx};
	auto tmp = Field{in.Nt, in.Nx};

	while (iterCount < maxIterations) {
		auto const oldNorm = r.Dot(r);

		applyDiracDagger(p, tmp);
		applyDirac(tmp, Ap);
		auto const alpha =  oldNorm / p.Dot(Ap);

		for (int t = 0; t < in.Nt; ++t) {
			for (int x = 0; x < in.Nx; ++x) {
				out(t, x) += alpha * p(t, x);
				r(t, x)   -= alpha * Ap(t, x);
			}
		}
		auto const newNorm = r.Dot(r);
		if (newNorm < CGtolerance) break;
		auto const beta = newNorm / oldNorm;

		for (int t = 0; t < in.Nt; ++t)
			for (int x = 0; x < in.Nx; ++x)
				p(t, x) = r(t, x) + beta * p(t, x);

		iterCount++;
	}

	return iterCount;
}

int main() {
	int constexpr Nt = 64;
	int constexpr Nx = 32;
	auto psi = Field{Nt, Nx};
	auto eta = Field{Nt, Nx};
	auto psi2 = Field{Nt, Nx};
	auto eta2 = Field{Nt, Nx};

	eta(0, 0) = s1;
	eta2(0, 0) = s2;

	std::cout << ConjugateGradient(eta, psi) << '\n';
	std::cout << ConjugateGradient(eta2, psi2) << '\n';
	applyDiracDagger(psi, eta);
	applyDiracDagger(psi2, eta2);

	std::vector<double> correlation;
	correlation.resize(Nt);

	for (int t = 0; t < Nt; ++t) {
		for (int x = 0; x < Nx; ++x)
			// computing correlator at zero momentum
			correlation[t] += s1.dot(eta(t, x)) + s2.dot(eta2(t, x));

		correlation[t] /= Nx;
		std::cout << t << '\t' << correlation[t] << '\n';
	}

	return 0;
}
