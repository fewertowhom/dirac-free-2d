EIGEN_PATH=/path/to/eigen

all: run-dirac

run-dirac: dirac2d.cpp
	g++ -std=c++11 -O2 -march=native -I${EIGEN_PATH} dirac2d.cpp -o run-dirac

clean:
	rm -rf run-dirac
